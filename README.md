# Mangatheque

# Installation

1. `npm i` || `yarn`
2. `npm run dev` || `yarn dev`
3. (optionnel) Si les ports ou l'api tourne sur un autre environnement que 'localhost:3000' sur un ngrok par exemple mettre a jour l'url de base de l'api en conséquence dans `main.js`

# Technologies

Vite3 + Vue3 pour le serveur et le framework  
TailwindCSS pour le framework css  
Axios pour réaliser les requêtes XHR  
Vue router pour le routage en mode SPA

# Architecture

## /public

Le dossier public, on y a laissé le logo de Vite présent lors de l'initialisation comme tribut

## /src

contient les différentes assets ainsi que certains scripts comme des composants vue ou des fonctions d'aide javascript

## /routes

La définition des routes utilisées par VueRouter

## /views

Les pages de l'application, un fichier vue représente une page, lesquelles sont rendues grâce au fichier `web.js` dans le dossier `routes`

## App.vue

Le niveau 0 de l'application qui s'occupe de charger le routeur notamment et d'avoir la navbar que l'on retrouve sur toute l'application

## main.js

L'instanciation de notre application et des différents plugins et variables globales (axios et store)

## store.js

Permet de partager efficacement des données au travers de l'application afin que l'application elle même possède un état. Le store utilisé est Pinia qui est plus moderne que Vuex.

## index.html

La page HTML qui va rendre contenir toute l'application, une fichier HTML pour en faire une SPA.

## tailwind.config.js

Le fichier de configuration de tailwind, permettant de créer des classes tailwind supplémentaires, gérer des polices, le mode de compilation utilisé et beaucoup d'autres choses.
