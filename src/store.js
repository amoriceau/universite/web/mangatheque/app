import {defineStore} from "pinia";

export const useStore = defineStore("store", {
    state: () => {
        return {
            user: null,
            auth: false,
            userToken: null
        };
    },
    getters: {},
    actions: {
        setUser(user, auth, token) {
            this.user = user
            this.auth = auth
            this.userToken = 'Bearer ' + token
        },
        removeUser(){
            this.user = null
            this.auth = false
            this.userToken = null
        }
    },
});
