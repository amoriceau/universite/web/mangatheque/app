import range from "./range.js";
export default function int2kanji(number) {
    const kanjis = {
        1: "一",
        2: "二",
        3: "三",
        4: "四",
        5: "五",
        6: "六",
        7: "七",
        8: "八",
        9: "九"
    };

    const digits = [
        "",
        "万",
        "億",
        "兆",
        "京",
        "垓",
        "𥝱",
        "穣",
        "溝",
        "澗",
        "正",
        "載",
        "極",
        "恒河沙",
        "阿僧祇",
        "那由多",
        "不可思議",
        "無量大数"
    ];

    const negative = number < 0;
    number = Math.abs(number);

    let result = "";
    range(Math.ceil(Math.log(number, 1000)), -1, -1).forEach((i) => {
        const num = (~~((number % 10 ** ((i + 1) * 4)) / 10 ** (i * 4)))
            .toString()
            .padStart(4, "0");
        let str = "";

        if (num === "0000") {
            return;
        }
        if (num[0] > "0") {
            if (num[0] !== "1") str += kanjis[+num[0]];
            str += "千";
        }
        if (num[1] > "0") {
            if (num[1] !== "1") str += kanjis[+num[1]];
            str += "百";
        }
        if (num[2] > "0") {
            if (num[2] !== "1") str += kanjis[+num[2]];
            str += "十";
        }
        if (num[3] > "0") {
            str += kanjis[+num[3]];
        }
        if (str) {
            result += str + digits[i];
        }
    });

    negative && (result = `-${result}`);
    return result;
};