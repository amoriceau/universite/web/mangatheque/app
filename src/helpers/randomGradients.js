export default () => {
    const gradients = [
        "background: #ffa17f; background: -webkit-linear-gradient(to right, #ffa17f, #00223e); background: linear-gradient(to right, #ffa17f, #00223e);",
        "background: #f953c6; background: -webkit-linear-gradient(to right, #b91d73, #f953c6); background: linear-gradient(to right, #b91d73, #f953c6);",
        "background: #000046; background: -webkit-linear-gradient(to right, #1CB5E0, #000046); background: linear-gradient(to right, #1CB5E0, #000046);",
        "background: #ff4b1f; background: -webkit-linear-gradient(to right, #1fddff, #ff4b1f); background: linear-gradient(to right, #1fddff, #ff4b1f);",
        "background: #556270; background: -webkit-linear-gradient(to right, #FF6B6B, #556270); background: linear-gradient(to right, #FF6B6B, #556270);",
        "background: #24C6DC; background: -webkit-linear-gradient(to bottom, #514A9D, #24C6DC);background: linear-gradient(to bottom, #514A9D, #24C6DC);",
        "background: #EC6F66; background: -webkit-linear-gradient(to left, #F3A183, #EC6F66);  background: linear-gradient(to left, #F3A183, #EC6F66);"
    ]

    return gradients[Math.floor(Math.random()*gradients.length)]
}