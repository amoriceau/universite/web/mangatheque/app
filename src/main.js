import { createApp } from 'vue'
import { createPinia } from "pinia";
import './style.css'
import App from './App.vue'
import axios from 'axios'
import {createRouter, createWebHistory} from "vue-router";

import web from "./routes/web.js"
import {useStore} from "./store.js";

const router = createRouter(
    {
        history: createWebHistory(),
        routes: web
    }
)

axios.defaults.baseURL = 'http://localhost:3000';
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
axios.defaults.headers.common['Access-Control-Allow-Origin'] = 'true';
axios.defaults.headers.common['Access-Control-Request-Headers'] = '*';


const app = createApp(App)
    .use(router)
    .use(createPinia())

const store = useStore()

app.config.globalProperties.$axios=axios
app.config.globalProperties.$store=store
app.mount('#app')