import Home from "../views/Home.vue";
import Navbar from "../views/Navbar.vue";
import Login from "../views/Login.vue";
import Register from "../views/Register.vue";
import Library from "../views/Library.vue";
import Discover from "../views/Discover.vue";
import Details from "../views/Mangas/Details.vue";
import PathNotFound from "../views/PathNotFound.vue"

export default [
    { path: '/', name: 'Home', component: Home },
    { path: '/navbar', name: 'Navbar', component: Navbar },
    { path: '/login', name: 'Login', component: Login },
    { path: '/register', name: 'Register', component: Register },
    { path: '/library', name: 'Library', component: Library },
    { path: '/discover', name: 'Discover', component: Discover },
	{ path: '/mangas/:id', name: 'MangaDetail', component: Details },
    { path: '/:pathMatch(.*)*', component: PathNotFound },
]