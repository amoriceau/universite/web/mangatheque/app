/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors:{
        'status-complete':  '#2ECC71',
        'status-owned':  '#BC002D',
        'main': '#7b52b9'
      },
    },
  },
  plugins: [require('@tailwindcss/forms'),
  ],
}